<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531145943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commande CHANGE date_commande date_commande VARCHAR(255) NOT NULL, CHANGE date_min_estime date_min_estime VARCHAR(255) NOT NULL, CHANGE date_max_estime date_max_estime VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commande CHANGE date_commande date_commande DATE NOT NULL, CHANGE date_min_estime date_min_estime DATE NOT NULL, CHANGE date_max_estime date_max_estime DATE NOT NULL');
    }
}
