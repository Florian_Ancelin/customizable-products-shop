<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210504144708 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE declinaison ADD produit_id INT DEFAULT NULL, DROP id_produit');
        $this->addSql('ALTER TABLE declinaison ADD CONSTRAINT FK_8E9AFFA1F347EFB FOREIGN KEY (produit_id) REFERENCES produit (id)');
        $this->addSql('CREATE INDEX IDX_8E9AFFA1F347EFB ON declinaison (produit_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE declinaison DROP FOREIGN KEY FK_8E9AFFA1F347EFB');
        $this->addSql('DROP INDEX IDX_8E9AFFA1F347EFB ON declinaison');
        $this->addSql('ALTER TABLE declinaison ADD id_produit INT NOT NULL, DROP produit_id');
    }
}
