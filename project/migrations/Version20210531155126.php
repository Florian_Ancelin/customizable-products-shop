<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531155126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ligne_commande ADD commande_id INT DEFAULT NULL, ADD declinaison_id INT DEFAULT NULL, ADD customization_id INT DEFAULT NULL, DROP id_commande, DROP id_decli, DROP id_customization');
        $this->addSql('ALTER TABLE ligne_commande ADD CONSTRAINT FK_3170B74B82EA2E54 FOREIGN KEY (commande_id) REFERENCES commande (id)');
        $this->addSql('ALTER TABLE ligne_commande ADD CONSTRAINT FK_3170B74B3D0575C1 FOREIGN KEY (declinaison_id) REFERENCES declinaison (id)');
        $this->addSql('ALTER TABLE ligne_commande ADD CONSTRAINT FK_3170B74BDE55AE3D FOREIGN KEY (customization_id) REFERENCES customization (id)');
        $this->addSql('CREATE INDEX IDX_3170B74B82EA2E54 ON ligne_commande (commande_id)');
        $this->addSql('CREATE INDEX IDX_3170B74B3D0575C1 ON ligne_commande (declinaison_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3170B74BDE55AE3D ON ligne_commande (customization_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ligne_commande DROP FOREIGN KEY FK_3170B74B82EA2E54');
        $this->addSql('ALTER TABLE ligne_commande DROP FOREIGN KEY FK_3170B74B3D0575C1');
        $this->addSql('ALTER TABLE ligne_commande DROP FOREIGN KEY FK_3170B74BDE55AE3D');
        $this->addSql('DROP INDEX IDX_3170B74B82EA2E54 ON ligne_commande');
        $this->addSql('DROP INDEX IDX_3170B74B3D0575C1 ON ligne_commande');
        $this->addSql('DROP INDEX UNIQ_3170B74BDE55AE3D ON ligne_commande');
        $this->addSql('ALTER TABLE ligne_commande ADD id_commande INT NOT NULL, ADD id_decli INT NOT NULL, ADD id_customization INT NOT NULL, DROP commande_id, DROP declinaison_id, DROP customization_id');
    }
}
