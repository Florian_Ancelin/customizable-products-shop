Feature: Panier
  Vérification que le panier est fonctionnel

  Scenario: Plusieurs produits sont ajouté au panier
    Given Un panier vide
    And plusieurs produits au hasard est ajouté au panier
    Then Le panier contient des produits

  Scenario: Plusieurs produits sont supprimé du panier
    Given Un panier contenant plusieurs produits
    And Les produits sont supprimé du panier
    Then Le panier est vide