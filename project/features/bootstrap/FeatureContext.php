<?php

use App\Controller\PanierController;
use Behat\Behat\Context\Context;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;
use function PHPUnit\Framework\isEmpty;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var Session
     */
    private Session $session;
    /**
     * @var PanierController
     */
    private PanierController $panierCtrl;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->session= new Session(new MockFileSessionStorage());
        $this->panierCtrl= new PanierController();
    }
    /**
     * @Given Un panier vide
     */
    public function unPanierVide()
    {
        $this->panierCtrl->getPanier($this->session);
    }

    /**
     * @Given plusieurs produits au hasard est ajouté au panier
     */
    public function plusieursProduitsAuHasardEstAjouteAuPanier()
    {
        $this->panierCtrl->add(750,$this->session);
        $this->panierCtrl->add(760,$this->session);
    }

    /**
     * @Then Le panier contient des produits
     */
    public function lePanierContientDesProduits()
    {
        $panier = $this->panierCtrl->getPanier($this->session);

        if (empty($panier)){
            throw new Exception("Le panier est vide");
        }
    }

    /**
     * @Given Un panier contenant plusieurs produits
     */
    public function unPanierContenantPlusieursProduits()
    {
        $this->panierCtrl->add(760,$this->session);
        $this->panierCtrl->add(770,$this->session);
    }

    /**
     * @Given Les produits sont supprimé du panier
     */
    public function lesProduitsSontSupprimeDuPanier()
    {
        $this->panierCtrl->remove(760,$this->session);
        $this->panierCtrl->remove(770,$this->session);
    }

    /**
     * @Then Le panier est vide
     */
    public function lePanierEstVide()
    {
        $panier = $this->panierCtrl->getPanier($this->session);

        if(!empty($panier)){
            throw new Exception("Le panier n'est pas vide");
        };
    }

}
