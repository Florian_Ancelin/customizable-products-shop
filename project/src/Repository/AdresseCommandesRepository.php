<?php

namespace App\Repository;

use App\Entity\AdresseCommandes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdresseCommandes|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdresseCommandes|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdresseCommandes[]    findAll()
 * @method AdresseCommandes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdresseCommandesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdresseCommandes::class);
    }

    // /**
    //  * @return AdresseCommandes[] Returns an array of AdresseCommandes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdresseCommandes
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
