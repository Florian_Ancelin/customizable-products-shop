<?php

namespace App\Faker;


use Faker\Provider;
use Faker\Generator;
use App\Faker\Provider\Produit;
use App\Faker\Provider\Adresse;
use App\Faker\Provider\Custom;

class ProviderCollection
{
    public static function addProdiverTo(Generator $faker)
    {
        $faker->addProvider(new Produit($faker));
        $faker->addProvider(new Adresse($faker));
        $faker->addProvider(new Custom($faker));
    }
}