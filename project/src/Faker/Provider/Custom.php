<?php


namespace App\Faker\Provider;

use Faker\Provider\Base;

class Custom extends Base
{
    protected static $position = ["Haut","Bas","Droite","Gauche"];
    protected static $type = ["Text","Photo"];
    protected static $personnalisation = ["Salut !","Florian", "J'aime le chocolat","HxH"];

    public function position(): string
    {
        return static::randomElement(static::$position);
    }

    public function personnalisation(): string
    {
        return static::randomElement(static::$personnalisation);
    }

    public function type(): string
    {
        return static::randomElement(static::$type);
    }
}