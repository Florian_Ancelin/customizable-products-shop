<?php


namespace App\Faker\Provider;

use Faker\Provider\Base;

class Adresse extends Base
{
    protected static $ville = ["Ambrines","Ambronay","Ambrugeat","Ambrumesnil","Ambrus","Ambutrix","Amécourt","Amel-sur-l'Etang","Amelécourt","Amélie-les-Bains-Palalda","Amendeuix-Oneix","Amenoncourt","Amenucourt","Ames","Amettes","Ameugny","Ameuvelle","Amfreville","Amfreville-la-Campagne","Amfreville-la-Mi-Voie","Amfreville-les-Champs","Amfreville-sous-les-Monts","Amfreville-sur-Iton"];
    protected static $departements = ["1","2","3","4","4", "5", "6", "7","84","13","92","77"];
    protected static $prerue = ["Rue","Impasse","Avenue", "Quartier","Boulevard","Route"];
    protected static $rue = ["des joncquilles", "du Stade", "de l'Arc de triomphe", "de De Gaulle", "de la Place", "de la Mairie", "Gaston Berger", "de Laporte"];
    protected static $numero = ["350","16","78","232","13","04", "56","19","94","85"];

    public function ville(): string
    {
        return static::randomElement(static::$ville);
    }

    public function departement(): string
    {
        return static::randomElement(static::$departements);
    }

    public function prerue(): string
    {
        return static::randomElement(static::$prerue);
    }

    public function rue(): string
    {
        return static::randomElement(static::$rue);
    }

    public function numero(): string
    {
        return static::randomElement(static::$numero);
    }
}