<?php

namespace App\Faker\Provider;

use Faker\Provider\Base;

class Produit extends Base
{
    protected static $libelleProduct = ["Tasse", "Bol", "Verre", "Gourde", "Bouteille", "Assiette", "Couverts", "Flute", "Tasse", "Dessous de plats", "Théière", "Spatule"];
    protected static $description = ["Lot de 6.","En fonte très solide.","A l'unitée","Image non contractuelle"," 15cm de hauteur", "9.5cm de diamètre", "En verre disponible dans plusieurs coloris dans la limite des stocks disponibles", "En crystal précieux, taillé à la main"];
    protected static $prix = ["12.99", "29.99", "99.99", "4.99", "9.99", "33.33", "72.50", "19.99","17.30"];
    protected static $image = ["https://cdn.habitat.fr/thumbnails/product/1073/1073287/width-height/518/270/tasse-a-cafe-en-porcelaine-blanche_1073287.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9sJRJjow5gZjMVYSb9NnvClfOsPnoBx3fcnkTIvN4WeFgwYD5yrg9vCo1YPvc9x4DYJmjYj8J&usqp=CAc"];
    protected static $personnalisable = ["Oui","Non"];

    public function libelleProduct(): string
    {
        return static::randomElement(static::$libelleProduct);
    }

    public function description(): string
    {
        return static::randomElement(static::$description);
    }

    public function prix(): float
    {
        return static::randomElement(static::$prix);
    }

    public function image(): string
    {
        return static::randomElement(static::$image);
    }

    public function personnalisable(): string
    {
        return static::randomElement(static::$personnalisable);
    }
}