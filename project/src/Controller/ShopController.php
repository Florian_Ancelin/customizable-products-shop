<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ShopController extends AbstractController
{
    function index(){
        return $this->redirect('http://localhost/catalogue');
    }

    /**
     * @Route("/catalogue", name="shop")
     * @param ProduitRepository $produitRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return mixed
     */
    function getPage(
        ProduitRepository $produitRepository,
        PaginatorInterface $paginator,
        Request $request
    )
    {
        $produits = $produitRepository->findAll();

        $mesProduits = $paginator
            ->paginate(
            $produits,
            $request->query->getInt('page',1),
            20
        );
        $mesProduits->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'large', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);
        return $this->render('accueil/accueil.html.twig', [
            'items'=> $mesProduits
        ]);
    }
}


