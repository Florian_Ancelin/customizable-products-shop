<?php

namespace App\Controller;

use App\Entity\AdresseCommandes;
use App\Entity\Commande;
use App\Entity\Customization;
use App\Entity\LigneCommande;
use App\Repository\AdresseUtilisateurRepository;
use App\Repository\CommandeRepository;
use App\Repository\DeclinaisonRepository;
use App\Repository\ProduitRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class CommandeController extends AbstractController
{
    /**
     * @Route("/commande/facturation", name="commande_facturation")
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @return Response
     */
    public function facturation(UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        $UserCtrl= new UserController();
        if(!$UserCtrl->checkUser($session)){
            return $this->redirect('http://localhost/catalogue');
        }

        return $this->render('commande/adrFacturation.html.twig', [
            'adresses' => $utilisateurRepository->findOneByEmail($session->get('email'))->getAdresseUtilisateurs(),
        ]);
    }

    /**
     * @Route("/commande/livraison", name="commande_livraison")
     * @param AdresseUtilisateurRepository $adresseUtilisateurRepository
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @return Response
     */
    public function validerAdrFacturation(AdresseUtilisateurRepository $adresseUtilisateurRepository,UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        $UserCtrl= new UserController();
        if(!$UserCtrl->checkUser($session)){
            return $this->redirect('http://localhost/catalogue');
        }

        if($_POST['type']=='auto') {
            $infos = $adresseUtilisateurRepository->find($_POST['adr_factu']);
            $session->set('facturation',[
                "pays"=>$infos->getPays(),
                "cp"=>$infos->getCodePostale(),
                "ville"=>$infos->getVille(),
                "rue"=>$infos->getLibelleAdresse(),
                "tel"=>$infos->getTel(),
                "type"=>"facturation"
            ]);
        }
        else if($_POST['type']=='manuel'){
            $session->set('facturation',[
                "pays"=>$_POST['pays'],
                "cp"=>(int)$_POST['cp'],
                "ville"=>$_POST['ville'],
                "rue"=>$_POST['libelle_adr'],
                "tel"=>$_POST['tel'],
                "type"=>"facturation"
            ]);
        }

        return $this->render('commande/adrLivraison.html.twig', [
            'adresses' => $utilisateurRepository->findOneByEmail($session->get('email'))->getAdresseUtilisateurs(),
        ]);
    }
    /**
     * @Route("/commande/payement", name="commande_payement")
     * @param AdresseUtilisateurRepository $adresseUtilisateurRepository
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @return Response
     */
    public function validerAdrLivraison(AdresseUtilisateurRepository $adresseUtilisateurRepository,UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        $UserCtrl= new UserController();
        if(!$UserCtrl->checkUser($session) || $session->get('facturation') == null){
            return $this->redirect('http://localhost/catalogue');
        }

        if($_POST['type']=='auto') {
            $infos = $adresseUtilisateurRepository->find($_POST['adr_livraison']);
            $session->set('livraison',[
                    "pays"=>$infos->getPays(),
                    "cp"=>$infos->getCodePostale(),
                    "ville"=>$infos->getVille(),
                    "rue"=>$infos->getLibelleAdresse(),
                    "tel"=>$infos->getTel(),
                    "type"=>"livraison"
            ]);
        }
        else if($_POST['type']=='manuel'){
            $session->set('livraison',[
                "pays"=>$_POST['pays'],
                "cp"=>(int)$_POST['cp'],
                "ville"=>$_POST['ville'],
                "rue"=>$_POST['libelle_adr'],
                "tel"=>$_POST['tel'],
                "type"=>"livraison"
            ]);
        }

        return $this->render('commande/payement.html.twig', [
            'adresses' => $utilisateurRepository->findOneByEmail($session->get('email'))->getAdresseUtilisateurs(),
        ]);
    }

    /**
     * @Route("/commande/recapitulatif", name="commande_recapitulation")
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @param EntityManagerInterface $manager
     * @param DeclinaisonRepository $declinaisonRepository
     * @return Response
     */
    public function validerCommande(UtilisateurRepository $utilisateurRepository,
                                    Session $session,
                                    EntityManagerInterface $manager,
                                    DeclinaisonRepository $declinaisonRepository): Response
    {
        $UserCtrl= new UserController();
        if(!$UserCtrl->checkUser($session) || $session->get('facturation') == null || $session->get('livraison') == null){
            return $this->redirect('http://localhost/catalogue');
        }
        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        setlocale(LC_TIME, 'fra_fra');
        // 2021-05-31 00:00:00
        $dateCommande = date("d/m/Y");
        $dateMin = date("d/m/Y", strtotime('+10 day', mktime(0, 0, 0, date('m'), date('d'), date('Y'))));
        $dateMax = date("d/m/Y", strtotime('+15 day', mktime(0, 0, 0, date('m'), date('d'), date('Y'))));

        $panier = $user->getSession();
        $adresses_f=$session->get('facturation');
        $adresses_l=$session->get('livraison');

        $panierCtrl = new PanierController();
        $livraison = new AdresseCommandes();
        $facturation = new AdresseCommandes();

        $livraison->setCodePostale($adresses_l['cp']);
        $livraison->setLibelleAdresse($adresses_l['rue']);
        $livraison->setTel($adresses_l['tel']);
        $livraison->setPays($adresses_l['pays']);
        $livraison->setVille($adresses_l['ville']);
        $livraison->setType($adresses_l['type']);

        $facturation->setCodePostale($adresses_f['cp']);
        $facturation->setLibelleAdresse($adresses_f['rue']);
        $facturation->setTel($adresses_f['tel']);
        $facturation->setPays($adresses_f['pays']);
        $facturation->setVille($adresses_f['ville']);
        $facturation->setType($adresses_f['type']);


        $commande = new Commande();
        $commande->setUtilisateur($user);
        $commande->setPrixTotal($panier['total']);
        $commande->setDateMinEstime($dateMin);
        $commande->setDateMaxEstime($dateMax);
        $commande->setDateCommande($dateCommande);
        $commande->addAdresseCommande($livraison);
        $commande->addAdresseCommande($facturation);
        $commande->setStatut('En attente');

        $livraison->setCommande($commande);
        $facturation->setCommande($commande);

        $manager->persist($commande);
        $manager->persist($facturation);
        $manager->persist($livraison);
        $manager->flush();

        foreach ($panier as $key => $value){
            if ($key != 'total'){
                foreach ($value as $type => $tab) {
                    if($type == 'base'){
                        $ligne = new LigneCommande();
                        $ligne->setQuantite($value['base']);
                        $ligne->setDeclinaison($declinaisonRepository->find($key));
                        $ligne->setCommande($commande);

                        $produits[$key]['base'] = [
                            "Produit" => $declinaisonRepository->find($key)->getIdProduit(),
                            "Quantity" => $value['base']
                        ];

                        $manager->persist($ligne);
                        $manager->flush();
                    }

                    if($type == 'custom'){
                        foreach ($tab as $it => $item ) {
                            $ligne = new LigneCommande();
                            $custom= new Customization();
                            $custom->setType($item['type']);
                            $custom->setPosition($item['position']);
                            $ligne->setQuantite($item['quantity']);
                            $ligne->setPersonnalisations($item['content']);
                            $ligne->setDeclinaison($declinaisonRepository->find($key));
                            $ligne->setCommande($commande);
                            $ligne->setCustomization($custom);
                            $custom->setLigneCommande($ligne);

                            $produits[$key]['custom'] = [
                                "Produit" => $declinaisonRepository->find($key)->getIdProduit(),
                                "Quantity" => $item['quantity']
                            ];

                            $manager->persist($ligne);
                            $manager->flush();
                        }
                    }
                }
            }
        }

        $session->remove('facturation');
        $session->remove('livraison');
        $session->remove('panier');

        $panierCtrl->updateSessionBDD($user,$session,$manager);

        return $this->render('commande/recap.html.twig', [
            'User'=> $utilisateurRepository->findOneByEmail($session->get('email')),
            'Produits' => $produits,
            'Commande' =>$commande,
            'Adresse'=>[
                'livraison'=>$livraison,
                'facturation'=>$facturation
            ],
            'Date' => [
                'min' => $dateMin,
                'max' => $dateMax
            ],
            'Total'  => $panier['total']
        ]);
    }

    /**
     * @Route(path = "/commande/consulter/{$id}", name = "viewCommande")
     * @param $id
     * @return mixed
     */
    public function consulteAction($id,CommandeRepository $commandeRepository)
    {
        return $this->render('commande/consultation.html.twig', [
            'commande' => $commandeRepository->find($id),
            'produits' => $commandeRepository->find($id)->getLigneCommandes()
        ]);
    }
}


