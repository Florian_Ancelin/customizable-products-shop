<?php


namespace App\Controller;

use App\Entity\Declinaison;
use App\Repository\DeclinaisonRepository;
use App\Repository\ProduitRepository;
use Doctrine\DBAL\Exception;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    /**
     * @Route("/produit/consulter/{id}", name="produit")
     * @param $id
     * @param ProduitRepository $produitRepository
     * @return mixed
     */
    public function index($id,ProduitRepository $produitRepository)
    {
        try {
            $monProd =$produitRepository->find($id);
            if($monProd==null){
                throw new Exception("Le produit demandé n'existe pas",404);
            }

            return $this->render('produit/produit.html.twig', [
                'produit'=> $monProd,
                'images'=>$monProd->getImages(),
                'declinaisons'=>$monProd->getDeclinaisons()
            ]);
        }
        catch (Exception $e){
            return $this->render('exception/exception.html.twig', [
                'erreur'=> "Erreur n°".$e->getCode().": ".$e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/produit/custom/{id}", name="produit_custom")
     * @param $id
     * @param DeclinaisonRepository $declinaisonRepository
     * @return mixed
     */
    public function custom($id, DeclinaisonRepository $declinaisonRepository)
    {
        return $this->render('produit/custom.html.twig',[
            'produit'=> $declinaisonRepository->find($id)->getIdProduit(),
            'decli'=> $declinaisonRepository->find($id)
        ]);
    }

    /**
     * @Route("/produit/validate/custom/{id}", name="produit_valide_custom")
     * @param $id
     * @param DeclinaisonRepository $declinaisonRepository
     * @return mixed
     */
    public function valideCustom($id, DeclinaisonRepository $declinaisonRepository)
    {
        return $this->redirect('http://localhost/panier');
    }
}