<?php

namespace App\Controller;

use App\Controller\Admin\DeclinaisonCrudController;
use App\Controller\Admin\UtilisateurCrudController;
use App\Entity\AdresseUtilisateur;
use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use PHPUnit\Util\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(): Response
    {
        return $this->render('account/index.html.twig', [
            'controller_name' => 'AccountController',
        ]);
    }

    /**
     * @Route("/account/login", name="account_login")
     * @param UtilisateurRepository $utilisateurRepository
     * @return Response
     */
    public function login(UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        if (isset($_POST['email']) || isset($_POST['password'])){
            $session->set('email',$_POST['email']);
            $session->set('password',$_POST['password']);
        }

        $email=$_POST['email'];
        $password=hash("sha256",$_POST['password']);
        try {
            if ($utilisateurRepository->findOneByEmail($email)==null||$utilisateurRepository->findOneByEmail($email)->getPassword()!=$password){
                self::disconnect($utilisateurRepository,$session);
                throw new Exception("Email ou mot de passe erroné",001);
            }
            else
                $session->set('role',$utilisateurRepository->findOneByEmail($_POST['email'])->getRole());
        }
        catch (Exception $e){
            return $this->render('exception/exception.html.twig', [
                'erreur'=> "Erreur n°".$e->getCode().": ".$e->getMessage()
            ]);
        }
        return $this->redirect('http://localhost/');
    }

    /**
     * @Route("/account/new", name="account_new")
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function newAccount(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager):Response
    {
        try {
            $email=$utilisateurRepository->findOneByEmail($_POST['email']);
            if($email!=null){
                throw new Exception("Email déjà lié à un compte",002);
            }
        }
        catch (Exception $e){
            return $this->render('exception/exception.html.twig', [
                'erreur'=> "Erreur n°".$e->getCode().": ".$e->getMessage()
            ]);
        }
        $adresse= new AdresseUtilisateur;
        $adresse->setLibelleAdresse($_POST['rue']);
        $adresse->setCodePostale($_POST['cp']);
        $adresse->setTel($_POST['tel']);
        $adresse->setPays("France");
        $adresse->setVille($_POST['commune']);

        $user = new Utilisateur;
        $user->setNomClient($_POST['nom']);
        $user->setPrenomClient($_POST['prenom']);
        $user->addAdresseUtilisateur($adresse);
        $user->setTel($_POST['tel']);
        $user->setPassword(hash("sha256",$_POST['password']));
        $user->setEmail($_POST['email']);
        $user->setRole("user");

        $adresse->setUser($user);


        $manager->persist($adresse);
        $manager->persist($user);
        $manager->flush();

        return $this->redirect('http://localhost/account');
    }

    /**
     * @Route("/account/deconnexion", name="account_deconnexion")
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @return Response
     */
    public function disconnect(UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        $session->remove('email');
        $session->remove('password');
        $session->remove('panier');
        $session->remove('role');

        return $this->redirect('http://localhost/');
    }

    /**
     * @Route(path = "/account/anone/{id}", name = "anonUser")
     * @param $id
     * @return mixed
     */
    public function anonAction($id,UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager)
    {
        $adresses =$utilisateurRepository->find($id)->getAdresseUtilisateurs();
        $commandes =$utilisateurRepository->find($id)->getCommandes();

        foreach ( $adresses as $adresse){
            $adresse->setUser($utilisateurRepository->findOneByEmail('LomeDepaye@imnothuman.com'));
            $adresse->setTel($utilisateurRepository->findOneByEmail('LomeDepaye@imnothuman.com')->getTel());
            $adresse->setLibelleAdresse($utilisateurRepository->findOneByEmail('LomeDepaye@imnothuman.com')->getAdresseUtilisateurs()->last()->getLibelleAdresse());
        }

        foreach ( $commandes as $commande){
            $commande->setUtilisateur($utilisateurRepository->findOneByEmail('LomeDepaye@imnothuman.com'));
        }
        $user = $utilisateurRepository->find($id);
        $manager->remove($user);
        $manager->flush();


        return $this->redirect('admin');
    }
}
