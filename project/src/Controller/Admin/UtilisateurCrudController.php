<?php

namespace App\Controller\Admin;

use App\Entity\Utilisateur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class UtilisateurCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Utilisateur::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('NomClient','Nom')->onlyOnIndex(),
            TextField::new('PrenomClient', 'Prénom')->onlyOnIndex(),
            TelephoneField::new('Tel','Téléphone')->onlyOnIndex(),
            EmailField::new('email','Mail')->onlyOnIndex(),
            ChoiceField::new('role')
                ->setChoices([
                    'user'=>'user',
                    'admin'=>'admin'])
        ];
    }
    public function configureActions(Actions $actions): Actions
    {

        $anonUser = Action::new('anonUser', 'Anonymiser les données utilisateur', 'fas fa-user-secret')
            // 2) using a callable (useful if parameters depend on the entity instance)
            // (the type-hint of the function argument is optional but useful)
            ->linkToRoute('anonUser',function (Utilisateur $utilisateur): array {
                return [
                    'id' => $utilisateur->getId(),
                ];
            } );

        return $actions
            ->add(Crud::PAGE_INDEX, $anonUser)
            ->disable(Action::NEW)
            ->disable(Action::DELETE);
    }
}
