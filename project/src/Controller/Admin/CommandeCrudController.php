<?php

namespace App\Controller\Admin;

use App\Entity\Commande;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommandeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commande::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new("Id")->onlyOnIndex(),
            TextField::new('DateCommande')->onlyOnIndex(),
            TextField::new('DateMinEstime')->onlyOnIndex(),
            TextField::new('DateMaxEstime')->onlyOnIndex(),
            IntegerField::new('PrixTotal')->onlyOnIndex(),

            ChoiceField::new('statut')
                ->setChoices([
                'En attente'=>'En attente',
                'Archivé'=>'Archivé',
                'En cours de traitement'=>'En cours de traitement'])
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        // if the method is not defined in a CRUD controller, link to its route
        $viewCommande = Action::new('viewCommande', 'Voir la commande', 'far fa-eye')
            // 2) using a callable (useful if parameters depend on the entity instance)
            // (the type-hint of the function argument is optional but useful)
            ->linkToRoute('viewCommande',function (Commande $commande): array {
                return [
                    'id' => $commande->getId(),
                ];
            } );

        return $actions
            ->add(Crud::PAGE_INDEX, $viewCommande)
            ->disable(Action::DELETE);
    }
}
