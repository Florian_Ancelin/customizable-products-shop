<?php

namespace App\Controller\Admin;

use App\Entity\Declinaison;
use App\Entity\Produit;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProduitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Produit::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            BooleanField::new('Disponible'),
            TextField::new('LibelleProduit'),
            TextEditorField::new('Description'),
            MoneyField::new('Prix')->setCurrency('EUR'),
            TextField::new('Image'),
            TextField::new('Personnalisable'),
            AssociationField::new('declinaisons')->onlyOnIndex(),
            AssociationField::new('images')->onlyOnIndex()
        ];
    }
}
