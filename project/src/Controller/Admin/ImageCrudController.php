<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Image::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('NomFichier', 'Nom du fichier'),
            ImageField::new('path','Fichier (au format jpg)')->setUploadDir('/public/images')->setUploadedFileNamePattern('[randomhash].[extension]'),
            AssociationField::new('produit')->autocomplete()
            ];
    }
}
