<?php

namespace App\Controller\Admin;

use App\Entity\Commande;
use App\Entity\Declinaison;
use App\Entity\Image;
use App\Entity\Produit;
use App\Entity\Utilisateur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use function mysql_xdevapi\getSession;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(DeclinaisonCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Customize your product shop');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section("Gestion des produits");
        yield MenuItem::linkToCrud('Déclinaisons','fas fa-palette',Declinaison::class);
        yield MenuItem::linkToCrud('Produits','fas fa-wine-glass',Produit::class);
        yield MenuItem::linkToCrud('Images','far fa-image',Image::class);

        yield MenuItem::section("Gestion des commandes");
        yield MenuItem::linkToCrud('Commande','fas fa-box-open',Commande::class);


        yield MenuItem::section("Gestion des clients");
        yield MenuItem::linkToCrud('Utilisateur','fas fa-users',Utilisateur::class);
    }

    public function checkPermissions(){

    }
}
