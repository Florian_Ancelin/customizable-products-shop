<?php

namespace App\Controller\Admin;

use App\Entity\Declinaison;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DeclinaisonCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Declinaison::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('libelle'),
            ColorField::new('coloris'),
            AssociationField::new('produit')
        ];
    }
}
