<?php

namespace App\Controller;

use App\Entity\AdresseUtilisateur;
use App\Repository\AdresseUtilisateurRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Util\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     * @param Session $session
     * @param UtilisateurRepository $user
     * @return Response
     */
    public function index(Session $session, UtilisateurRepository $user): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        return $this->render('user/index.html.twig', [
            'user' => $user->findOneByEmail($session->get('email'))
        ]);
    }

    /**
     * @Route("/user/datamodifier/{donnee}", name="user_modifier")
     * @param Session $session
     * @param $donnee
     * @return Response
     */
    public function modifier(Session $session, $donnee): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        return $this->render('user/modif/' . strtolower($donnee) . '.html.twig');
    }

    /**
     * @Route("/user/modifier/nom", name="user_modifier_nom")
     * @param Session $session
     * @return Response
     */
    public function modifierNom(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        try {
            if (!self::checkUser($session))
                return $this->redirect("http://localhost/catalogue");

            $user = $utilisateurRepository->findOneByEmail($session->get('email'));
            if(!isset($_POST['nom']))
                throw new Exception("Une erreur est survenue veuillez rééssayer",555);

            $user->setNomClient($_POST['nom']);
            $manager->persist($user);
            $manager->flush();

            return $this->redirect("http://localhost/user");
        }
        catch(Exception $e){
            return $this->render('exception/exception.html.twig', [
                'erreur'=> "Erreur n°".$e->getCode().": ".$e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/user/modifier/prenom", name="user_modifier_prenom")
     * @param Session $session
     * @return Response
     */
    public function modifierPrenom(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->setPrenomClient($_POST['prenom']);
        $manager->persist($user);
        $manager->flush();

        return $this->redirect("http://localhost/user");
    }

    /**
     * @Route("/user/modifier/adresse", name="user_modifier_adresse")
     * @param Session $session
     * @return Response
     */
    public function modifierAdresse(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->setAdresseClient($_POST['adresse']);
        $manager->persist($user);
        $manager->flush();

        return $this->redirect("http://localhost/user");
    }

    /**
     * @Route("/user/modifier/tel", name="user_modifier_tel")
     * @param Session $session
     * @return Response
     */
    public function modifierTel(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->setTel($_POST['tel']);
        $manager->persist($user);
        $manager->flush();

        return $this->redirect("http://localhost/user");
    }

    /**
     * @Route("/user/modifier/email", name="user_modifier_email")
     * @param Session $session
     * @return Response
     */
    public function modifierEmail(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->setEmail($_POST['email']);
        $manager->persist($user);
        $manager->flush();

        return $this->redirect("http://localhost/user");
    }

    /**
     * @Route("/user/modifier/password", name="user_modifier_password")
     * @param Session $session
     * @return Response
     */
    public function modifierPassword(UtilisateurRepository $utilisateurRepository, EntityManagerInterface $manager, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->setPassword(hash("sha256",$_POST['password']));
        $manager->persist($user);
        $manager->flush();

        return $this->redirect("http://localhost/user");
    }

    /**
     * @Route("/user/adresse", name="user_adresse")
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @return Response
     */
    public function voirAdresse(UtilisateurRepository $utilisateurRepository, Session $session): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        return $this->render('user/adresse.html.twig', [
            'adresses' => $user->getAdresseUtilisateurs()
        ]);
    }

    /**
     * @Route("/user/adresse/remove/{adresse_id}", name="user_adresse_remove")
     * @param UtilisateurRepository $utilisateurRepository
     * @param AdresseUtilisateurRepository $adresseUtilisateurRepository
     * @param Session $session
     * @param EntityManagerInterface $manager
     * @param $adresse_id
     * @return Response
     */
    public function removeAdresse(UtilisateurRepository $utilisateurRepository,AdresseUtilisateurRepository $adresseUtilisateurRepository, Session $session,EntityManagerInterface $manager,$adresse_id): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");
        elseif ($session->get('email') != $adresseUtilisateurRepository->find($adresse_id)->getUser()->getEmail())
            return $this->redirect("http://localhost/catalogue");

        $user = $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->removeAdresseUtilisateur($adresseUtilisateurRepository->find($adresse_id));
        $manager->persist($user);
        $manager->flush();

        return $this->render('user/adresse.html.twig', [
            'adresses' => $user->getAdresseUtilisateurs()
        ]);
    }

    /**
     * @Route("/user/adresse/add", name="user_adresse_add")
     * @param UtilisateurRepository $utilisateurRepository
     * @param Session $session
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function addAdresse(UtilisateurRepository $utilisateurRepository, Session $session,EntityManagerInterface $manager): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $adresse = new AdresseUtilisateur();
        $adresse->setUser($utilisateurRepository->findOneByEmail($session->get('email')));
        $adresse->setVille($_POST['ville']);
        $adresse->setPays($_POST['pays']);
        $adresse->setTel($_POST['tel']);
        $adresse->setLibelleAdresse($_POST['libelle_adr']);
        $adresse->setCodePostale($_POST['cp']);

        $user= $utilisateurRepository->findOneByEmail($session->get('email'));
        $user->addAdresseUtilisateur($adresse);

        $manager->persist($adresse);
        $manager->persist($user);
        $manager->flush();

        return $this->render('user/adresse.html.twig', [
            'adresses' => $user->getAdresseUtilisateurs()
        ]);
    }

    /**
     * @Route("/user/commande", name="user_commande_history")
     * @param Session $session
     * @return Response
     */
    public function voirCommandes(Session $session, UtilisateurRepository $utilisateurRepository): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $commandes = $utilisateurRepository->findOneByEmail($session->get('email'))->getCommandes();

        return $this->render('user/commandes.html.twig', [
            'Commandes' => $commandes
        ]);
    }

    /**
     * @Route("/user/commande/{commande_id}", name="user_commande")
     * @param Session $session
     * @param UtilisateurRepository $utilisateurRepository
     * @return Response
     */
    public function voirCommande(Session $session, UtilisateurRepository $utilisateurRepository, $commande_id): Response
    {
        if (!self::checkUser($session))
            return $this->redirect("http://localhost/catalogue");

        $commandes = $utilisateurRepository->findOneByEmail($session->get('email'))->getCommandes();
        $ismycommande = false ;
        foreach ( $commandes as $key => $value){
            if ($value->getId()==$commande_id) {
                $ismycommande = true;
                $mycommande = $value;
            }
        }
        if (!$ismycommande)
            return $this->redirect("http://localhost/catalogue");

        $myproducts = $mycommande->getLigneCommandes();
        $arrProd = [];
        foreach ( $myproducts as $key => $value){
            if($value->getCustomization() != null) {
                $arrProd[$key] = [
                    'quantite' => $value->getQuantite(),
                    'produit'=> $value->getDeclinaison()->getIdProduit(),
                    'decli' => $value->getDeclinaison(),
                    'custom' => [
                        'content' => $value->getPersonnalisations(),
                        'type' => $value->getCustomization()->getType(),
                        'position' => $value->getCustomization()->getPosition()
                    ],
                ];
            }
            else{
                $arrProd[$key] = [
                    'quantite' => $value->getQuantite(),
                    'produit'=> $value->getDeclinaison()->getIdProduit(),
                    'decli' => $value->getDeclinaison(),
                ];
            }
        }

        return $this->render('user/commande.html.twig', [
            'total'=> $mycommande->getPrixTotal(),
            'Adresses'=>$mycommande->getAdresseCommandes(),
            'Produits' => $arrProd
        ]);
    }

    public function checkUser(Session $session): bool
    {
        return $session->get('email') != null;
    }

}
