<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Repository\DeclinaisonRepository;
use App\Repository\ProduitRepository;
use App\Repository\SessionRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     * @param Session $session
     * @param DeclinaisonRepository $declinaisonRepository
     * @param ProduitRepository $produitRepository
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return mixed
     */
    public function index(Session $session,
                          DeclinaisonRepository $declinaisonRepository,
                          ProduitRepository $produitRepository,
                          UtilisateurRepository $utilisateurRepository,
                          EntityManagerInterface $manager)
    {
        $panier = self::getPanier($session, $utilisateurRepository, $manager);

        $dataPanier = [];
        $customPanier = [];


        foreach ($panier as $id => $tab) {
            if ($declinaisonRepository->find($id) == null){
                unset($panier[$id]);
                self::setPanier($panier,$session,$utilisateurRepository,$manager);
            }
            if ($id != "total") {
                if (!$declinaisonRepository->find($id)->getIdProduit()->getDisponible()) {
                    unset($panier[$id]);
                    self::setPanier($panier, $session, $utilisateurRepository, $manager);
                }
            }
            if ($id != "total" && $declinaisonRepository->find($id)->getIdProduit()->getDisponible()) {
                if (array_key_exists('base', $tab)) {
                    $dataPanier[] = [
                        'produit' => $declinaisonRepository->find($id)->getIdProduit(),
                        'quantity' => $tab['base'],
                        'type' => 'base',
                        'declinaison' => $declinaisonRepository->find($id)
                    ];
                }
                if (array_key_exists('custom', $tab)) {
                    for ($i = 0; $i < sizeof($tab['custom']); ++$i) {
                        $customPanier[] = [
                            'produit' => $declinaisonRepository->find($id)->getIdProduit(),
                            'content' => $tab['custom'][$i],
                            'quantity' => $tab['custom'][$i]['quantity'],
                            'type' => 'custom',
                            'declinaison' => $declinaisonRepository->find($id)
                        ];
                    }
                }
            }
        }

        $total = 0;

        foreach ($dataPanier as $item) {
            $totalItem = $item['produit']->getPrix() * $item['quantity'];
            $total += $totalItem;
        }
        foreach ($customPanier as $item) {
            $totalItem = $item['produit']->getPrix() * $item['quantity'];
            $total += $totalItem;
        }

        $panier['total'] = $total;
        self::setPanier($panier, $session, $utilisateurRepository, $manager);

        return $this->render('panier/index.html.twig', [
            'items' => $dataPanier,
            'itemsCustom' => $customPanier,
            'prixTotal' => $total
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="panier_add")
     * @param $id
     * @param Session $session
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return mixed
     */
    public function add($id,
                        Session $session,
                        UtilisateurRepository $utilisateurRepository,
                        EntityManagerInterface $manager,
                        DeclinaisonRepository $declinaisonRepository)
    {
        if(!$declinaisonRepository->find($id)->getProduit()->getDisponible()){
                return $this->render('exception/exception.html.twig', [
                    'erreur'=> "Erreur n°0002: Produit indisponible"
                ]);
        }
        $panier = self::getPanier($session, $utilisateurRepository, $manager);

        if (!empty($panier[$id]['base'])) {
            $panier[$id]['base']++;
        } else {
            $panier[$id]['base'] = 1;
        }
        self::setPanier($panier, $session, $utilisateurRepository, $manager);
        return $this->redirect('http://localhost/');
    }

    /**
     * @Route("/panier/addCustom/{id}", name="panier_add_custom")
     * @param $id
     * @param Session $session
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return mixed
     */
    public function add_custom($id,
                               Session $session,
                               UtilisateurRepository $utilisateurRepository,
                               EntityManagerInterface $manager)
    {

        $panier = self::getPanier($session, $utilisateurRepository, $manager);

        if (!empty($panier[$id]['base']) && $panier[$id]['base'] == 1) {
            unset($panier[$id]['base']);
        } elseif (!empty($panier[$id]['base']) && $panier[$id]['base'] > 1) {
            $panier[$id]['base']--;
        }

        if (!isset($panier[$id]['custom']))
            $panier[$id]['custom'] = [];

        if ($_POST['type'] == 'img') {
            if ($_FILES['image']['error'] == 0 and $_FILES['image']['size'] < 2097152) {
                $file = '<img src="data:' . $_FILES['image']['type'] .
                     ';base64,' . base64_encode(file_get_contents($_FILES['image']['tmp_name'])) .
                     '" alt="' . $_FILES['image']['name'] . '"/>';

                $panier[$id]['custom'][sizeof($panier[$id]['custom'])] = [
                    'type' => $_POST['type'],
                    'position' => $_POST['position'],
                    'content' => $file,
                    'quantity' => 1
                ];
            }
            else {
                return $this->render('exception/exception.html.twig', [
                    'erreur'=> "Erreur : Veuillez vérifiez que votre image n'excède pas 2Mo "
                ]);
            }
        } elseif ($_POST['type'] == 'txt') {
            $panier[$id]['custom'][sizeof($panier[$id]['custom'])] = [
                'type' => $_POST['type'],
                'position' => $_POST['position'],
                'content' => $_POST['msg'],
                'quantity' => 1
            ];
        }

        self::setPanier($panier, $session, $utilisateurRepository, $manager);
        return $this->redirect('http://localhost/');
    }


    /**
     * @Route("/panier/remove/{id}", name="panier_remove")
     * @param $id
     * @param Session $session
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return mixed
     */
    public function remove($id,
                           Session $session,
                           UtilisateurRepository $utilisateurRepository,
                           EntityManagerInterface $manager)
    {

        $panier = self::getPanier($session, $utilisateurRepository, $manager);

        if (!empty($panier[$id]['base'])) {
            unset($panier[$id]['base']);
            if (empty($panier[$id])) {
                unset($panier[$id]);
            }
        }

        self::setPanier($panier, $session, $utilisateurRepository, $manager);

        return $this->redirect('http://localhost/panier');
    }

    /**
     * @Route("/panier/removeCustom/{id}/{key}", name="panier_remove_custom")
     * @param $id
     * @param Session $session
     * @param UtilisateurRepository $utilisateurRepository
     * @param EntityManagerInterface $manager
     * @return mixed
     */
    public function removeCustom($id,
                                 $key,
                                 Session $session,
                                 UtilisateurRepository $utilisateurRepository,
                                 EntityManagerInterface $manager)
    {

        $panier = self::getPanier($session, $utilisateurRepository, $manager);

        if (!empty($panier[$id]['custom'][$key])) {
            unset($panier[$id]['custom'][$key]);
            if (empty($panier[$id]['custom'])) {
                unset($panier[$id]['custom']);
                if (empty($panier[$id])) {
                    unset($panier[$id]);
                }
            } else {
                $newtab = [];
                foreach ($panier[$id]['custom'] as $custom) {
                    array_push($newtab, $custom);
                }
                $panier[$id]['custom'] = $newtab;
            }
        }

        self::setPanier($panier, $session, $utilisateurRepository, $manager);

        return $this->redirect('http://localhost/panier');
    }


    public
    function getPanier(Session $session,
                       UtilisateurRepository $utilisateurRepository,
                       EntityManagerInterface $manager)
    {

        $userCtrl = new UserController();
        if ($userCtrl->checkUser($session)) {
            $user = $utilisateurRepository->findOneByEmail($session->get('email'));
            if ($user->getSession() == null) {
                self::updateSessionBDD($user, $session, $manager);
            } else
                self::setPanier($user->getSession(), $session, $utilisateurRepository, $manager);
        }
        return $session->get('panier', []);
    }

    public
    function setPanier($data,
                       Session $session,
                       UtilisateurRepository $utilisateurRepository,
                       EntityManagerInterface $manager)
    {
        $session->set('panier', $data);
        $userCtrl = new UserController();
        if ($userCtrl->checkUser($session)) {
            self::updateSessionBDD($utilisateurRepository->findOneByEmail($session->get('email')), $session, $manager);
        }
    }

    public
    function updateSessionBDD(Utilisateur $user,
                              Session $session,
                              EntityManagerInterface $manager)
    {

        $user->setSession($session->get('panier', []));
        $manager->persist($user);
        $manager->flush();
    }
}
