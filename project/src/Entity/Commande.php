<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CommandeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 * @ApiResource()
 */
class Commande
{
    const STATUT_EN_ATTENTE ='En attente';
    const STATUT_ARCHIVE  ='Archivé';
    const STATUT_EN_TRAITEMENT ='En cours de traitement';


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $DateCommande;

    /**
     * @ORM\Column(type="string")
     */
    private $DateMinEstime;

    /**
     * @ORM\Column(type="string")
     */
    private $DateMaxEstime;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $PrixTotal;

    /**
     * @ORM\OneToMany(targetEntity=AdresseCommandes::class, mappedBy="Commande")
     */
    private $adresseCommandes;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="commandes")
     */
    private $Utilisateur;

    /**
     * @ORM\OneToMany(targetEntity=LigneCommande::class, mappedBy="Commande")
     */
    private $ligneCommandes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Statut;

    public function __construct()
    {
        $this->Pays = new ArrayCollection();
        $this->adresseCommandes = new ArrayCollection();
        $this->ligneCommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCommande(): ?string
    {
        return $this->DateCommande;
    }

    public function setDateCommande(string $DateCommande): self
    {
        $this->DateCommande = $DateCommande;

        return $this;
    }

    public function getDateMinEstime(): ?string
    {
        return $this->DateMinEstime;
    }

    public function setDateMinEstime(string $DateMinEstime): self
    {
        $this->DateMinEstime = $DateMinEstime;

        return $this;
    }

    public function getDateMaxEstime(): ?string
    {
        return $this->DateMaxEstime;
    }

    public function setDateMaxEstime(string $DateMaxEstime): self
    {
        $this->DateMaxEstime = $DateMaxEstime;

        return $this;
    }

    public function getPrixTotal(): ?string
    {
        return $this->PrixTotal;
    }

    public function setPrixTotal(string $PrixTotal): self
    {
        $this->PrixTotal = $PrixTotal;

        return $this;
    }

    /**
     * @return Collection|AdresseCommandes[]
     */
    public function getAdresseCommandes(): Collection
    {
        return $this->adresseCommandes;
    }

    public function addAdresseCommande(AdresseCommandes $adresseCommande): self
    {
        if (!$this->adresseCommandes->contains($adresseCommande)) {
            $this->adresseCommandes[] = $adresseCommande;
            $adresseCommande->setCommande($this);
        }

        return $this;
    }

    public function removeAdresseCommande(AdresseCommandes $adresseCommande): self
    {
        if ($this->adresseCommandes->removeElement($adresseCommande)) {
            // set the owning side to null (unless already changed)
            if ($adresseCommande->getCommande() === $this) {
                $adresseCommande->setCommande(null);
            }
        }

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->Utilisateur;
    }

    public function setUtilisateur(?Utilisateur $Utilisateur): self
    {
        $this->Utilisateur = $Utilisateur;

        return $this;
    }

    /**
     * @return Collection|LigneCommande[]
     */
    public function getLigneCommandes(): Collection
    {
        return $this->ligneCommandes;
    }

    public function addLigneCommande(LigneCommande $ligneCommande): self
    {
        if (!$this->ligneCommandes->contains($ligneCommande)) {
            $this->ligneCommandes[] = $ligneCommande;
            $ligneCommande->setCommande($this);
        }

        return $this;
    }

    public function removeLigneCommande(LigneCommande $ligneCommande): self
    {
        if ($this->ligneCommandes->removeElement($ligneCommande)) {
            // set the owning side to null (unless already changed)
            if ($ligneCommande->getCommande() === $this) {
                $ligneCommande->setCommande(null);
            }
        }

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->Statut;
    }

    public function setStatut(string $Statut): self
    {
        if(!in_array($Statut, array(self::STATUT_ARCHIVE, self::STATUT_EN_ATTENTE, self::STATUT_EN_TRAITEMENT))){
            throw new \InvalidArgumentException("Statut invalide");
        }

        $this->Statut = $Statut;
        return $this;
    }
}