<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LigneCommandeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LigneCommandeRepository::class)
 * @ApiResource()
 */
class LigneCommande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Commande::class, inversedBy="ligneCommandes")
     */
    private $Commande;

    /**
     * @ORM\ManyToOne(targetEntity=Declinaison::class, inversedBy="ligneCommandes", cascade={"persist"})
     */
    private $Declinaison;

    /**
     * @ORM\OneToOne(targetEntity=Customization::class, inversedBy="ligneCommande", cascade={"persist", "remove"})
     */
    private $Customization;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Personnalisations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->Quantite;
    }

    public function setQuantite(int $Quantite): self
    {
        $this->Quantite = $Quantite;

        return $this;
    }

    public function getCommande(): ?Commande
    {
        return $this->Commande;
    }

    public function setCommande(?Commande $Commande): self
    {
        $this->Commande = $Commande;

        return $this;
    }

    public function getDeclinaison(): ?Declinaison
    {
        return $this->Declinaison;
    }

    public function setDeclinaison(?Declinaison $Declinaison): self
    {
        $this->Declinaison = $Declinaison;

        return $this;
    }

    public function getCustomization(): ?Customization
    {
        return $this->Customization;
    }

    public function setCustomization(?Customization $Customization): self
    {
        $this->Customization = $Customization;

        return $this;
    }

    public function getPersonnalisations(): ?string
    {
        return $this->Personnalisations;
    }

    public function setPersonnalisations(?string $Personnalisations): self
    {
        $this->Personnalisations = $Personnalisations;

        return $this;
    }
}
