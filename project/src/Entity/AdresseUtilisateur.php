<?php

namespace App\Entity;

use App\Repository\AdresseUtilisateurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseUtilisateurRepository::class)
 */
class AdresseUtilisateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="adresseUtilisateurs")
     */
    private $User;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Pays;

    /**
     * @ORM\Column(type="integer")
     */
    private $CodePostale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LibelleAdresse;

    /**
     * @ORM\Column(type="text")
     */
    private $Tel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Utilisateur
    {
        return $this->User;
    }

    public function setUser(?Utilisateur $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->Pays;
    }

    public function setPays(string $Pays): self
    {
        $this->Pays = $Pays;

        return $this;
    }

    public function getCodePostale(): ?int
    {
        return $this->CodePostale;
    }

    public function setCodePostale(int $CodePostale): self
    {
        $this->CodePostale = $CodePostale;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function getLibelleAdresse(): ?string
    {
        return $this->LibelleAdresse;
    }

    public function setLibelleAdresse(string $LibelleAdresse): self
    {
        $this->LibelleAdresse = $LibelleAdresse;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->Tel;
    }

    public function setTel(string $Tel): self
    {
        $this->Tel = $Tel;

        return $this;
    }
}
