<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\DeclinaisonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeclinaisonRepository::class)
 * @ApiResource(
 *
 * )
 *
 * @ApiFilter(
 *     SearchFilter::class,properties={"produit":"exact"}
 * )
 */
class Declinaison
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $coloris;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="declinaisons", cascade={"persist"})
     */
    private $produit;

    /**
     * @ORM\OneToMany(targetEntity=LigneCommande::class, mappedBy="Declinaison", cascade={"remove"}, orphanRemoval=true)
     */
    private $ligneCommandes;

    public function __construct()
    {
        $this->ligneCommandes = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $Libelle): self
    {
        $this->libelle = $Libelle;

        return $this;
    }

    public function getColoris(): ?string
    {
        return $this->coloris;
    }

    public function setColoris(string $Coloris): self
    {
        $this->coloris = $Coloris;

        return $this;
    }

    public function getIdProduit(): ?Produit
    {
        return $this->produit;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function setIdProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * @return Collection|LigneCommande[]
     */
    public function getLigneCommandes(): Collection
    {
        return $this->ligneCommandes;
    }

    public function addLigneCommande(LigneCommande $ligneCommande): self
    {
        if (!$this->ligneCommandes->contains($ligneCommande)) {
            $this->ligneCommandes[] = $ligneCommande;
            $ligneCommande->setDeclinaison($this);
        }

        return $this;
    }

    public function removeLigneCommande(LigneCommande $ligneCommande): self
    {
        if ($this->ligneCommandes->removeElement($ligneCommande)) {
            // set the owning side to null (unless already changed)
            if ($ligneCommande->getDeclinaison() === $this) {
                $ligneCommande->setDeclinaison(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->libelle;
    }
}
