<?php

namespace App\Entity;

use App\Repository\AdresseCommandesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseCommandesRepository::class)
 */
class AdresseCommandes
{
    const TYPE_LIVRAISON ='livraison';
    const TYPE_FACTURATION  ='facturation';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Commande::class, inversedBy="adresseCommandes")
     */
    private $Commande;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Pays;

    /**
     * @ORM\Column(type="integer")
     */
    private $CodePostale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LibelleAdresse;

    /**
     * @ORM\Column(type="text")
     */
    private $Tel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommande(): ?Commande
    {
        return $this->Commande;
    }

    public function setCommande(?Commande $Commande): self
    {
        $this->Commande = $Commande;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->Pays;
    }

    public function setPays(string $Pays): self
    {
        $this->Pays = $Pays;

        return $this;
    }

    public function getCodePostale(): ?int
    {
        return $this->CodePostale;
    }

    public function setCodePostale(int $CodePostale): self
    {
        $this->CodePostale = $CodePostale;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function getLibelleAdresse(): ?string
    {
        return $this->LibelleAdresse;
    }

    public function setLibelleAdresse(string $LibelleAdresse): self
    {
        $this->LibelleAdresse = $LibelleAdresse;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->Tel;
    }

    public function setTel(string $Tel): self
    {
        $this->Tel = $Tel;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        if(!in_array($Type, array(self::TYPE_FACTURATION, self::TYPE_LIVRAISON))){
            throw new \InvalidArgumentException("Type invalide");
        }

        $this->Type = $Type;
        return $this;
    }
}
