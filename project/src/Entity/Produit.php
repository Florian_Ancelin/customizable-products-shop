<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitRepository::class)
 * @ApiResource(attributes={"pagination_enabled"=false})
 */
class Produit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelleProduit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $personnalisable;

    /**
     *
     * @ORM\OneToMany(targetEntity=Declinaison::class, mappedBy="produit", cascade={"remove"}, orphanRemoval=true)
     */
    private $declinaisons;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="produit", cascade={"remove"}, orphanRemoval=true)
     */
    private $images;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Disponible;

    public function __construct()
    {
        $this->declinaisons = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleProduit(): ?string
    {
        return $this->libelleProduit;
    }

    public function setLibelleProduit(string $libelleProduit): self
    {
        $this->libelleProduit = $libelleProduit;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $Description): self
    {
        $this->description = $Description;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $Prix): self
    {
        $this->prix = $Prix;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $Image): self
    {
        $this->image = $Image;

        return $this;
    }

    public function getPersonnalisable(): ?string
    {
        return $this->personnalisable;
    }

    public function setPersonnalisable(string $Personnalisable): self
    {
        $this->personnalisable = $Personnalisable;

        return $this;
    }

    /**
     * @return Collection|Declinaison[]
     */
    public function getDeclinaisons(): Collection
    {
        return $this->declinaisons;
    }

    public function addDeclinaison(Declinaison $declinaison): self
    {
        if (!$this->declinaisons->contains($declinaison)) {
            $this->declinaisons[] = $declinaison;
            $declinaison->setProduit($this);
        }

        return $this;
    }

    public function removeDeclinaison(Declinaison $declinaison): self
    {
        if ($this->declinaisons->removeElement($declinaison)) {
            // set the owning side to null (unless already changed)
            if ($declinaison->getProduit() === $this) {
                $declinaison->setProduit(null);
            }
        }

        return $this;
    }
    public function __toString(): string
    {
        return $this->libelleProduit;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduit($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getProduit() === $this) {
                $image->setProduit(null);
            }
        }

        return $this;
    }

    public function getDisponible(): ?bool
    {
        return $this->Disponible;
    }

    public function setDisponible(bool $Disponible): self
    {
        $this->Disponible = $Disponible;

        return $this;
    }
}
