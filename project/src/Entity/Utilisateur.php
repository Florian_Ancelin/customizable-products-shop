<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 * @ApiResource()
 */
class Utilisateur
{
    const ROLE_GUEST ='guest';
    const ROLE_USER  ='user';
    const ROLE_ADMIN ='admin';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NomClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PrenomClient;

    /**
     * @ORM\Column(type="text")
     */
    private $Tel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Role;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $Session = [];

    /**
     * @ORM\OneToMany(targetEntity=AdresseUtilisateur::class, mappedBy="User")
     */
    private $adresseUtilisateurs;

    /**
     * @ORM\OneToMany(targetEntity=Commande::class, mappedBy="Utilisateur")
     */
    private $commandes;

    public function __construct()
    {
        $this->adresseUtilisateurs = new ArrayCollection();
        $this->commandes = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomClient(): ?string
    {
        return $this->NomClient;
    }

    public function setNomClient(string $NomClient): self
    {
        $this->NomClient = $NomClient;

        return $this;
    }

    public function getPrenomClient(): ?string
    {
        return $this->PrenomClient;
    }

    public function setPrenomClient(string $PrenomClient): self
    {
        $this->PrenomClient = $PrenomClient;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->Tel;
    }

    public function setTel(string $Tel): self
    {
        $this->Tel = $Tel;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->Password;
    }

    public function setPassword(string $Password): self
    {
        $this->Password = $Password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->Role;
    }

    public function setRole(string $Role): self
    {
        if(!in_array($Role, array(self::ROLE_GUEST, self::ROLE_USER, self::ROLE_ADMIN))){
            throw new \InvalidArgumentException("Role invalide");
        }

        $this->Role = $Role;
        return $this;
    }

    public function getSession(): ?array
    {
        return $this->Session;
    }

    public function setSession(?array $Session): self
    {
        $this->Session = $Session;

        return $this;
    }

    /**
     * @return Collection|AdresseUtilisateur[]
     */
    public function getAdresseUtilisateurs(): Collection
    {
        return $this->adresseUtilisateurs;
    }

    public function addAdresseUtilisateur(AdresseUtilisateur $adresseUtilisateur): self
    {
        if (!$this->adresseUtilisateurs->contains($adresseUtilisateur)) {
            $this->adresseUtilisateurs[] = $adresseUtilisateur;
            $adresseUtilisateur->setUser($this);
        }

        return $this;
    }

    public function removeAdresseUtilisateur(AdresseUtilisateur $adresseUtilisateur): self
    {
        if ($this->adresseUtilisateurs->removeElement($adresseUtilisateur)) {
            // set the owning side to null (unless already changed)
            if ($adresseUtilisateur->getUser() === $this) {
                $adresseUtilisateur->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setUtilisateur($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getUtilisateur() === $this) {
                $commande->setUtilisateur(null);
            }
        }

        return $this;
    }
}
