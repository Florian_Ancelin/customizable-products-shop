<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CustomizationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomizationRepository::class)
 * @ApiResource()
 */
class Customization
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Position;

    /**
     * @ORM\OneToOne(targetEntity=LigneCommande::class, mappedBy="Customization", cascade={"persist", "remove"})
     */
    private $ligneCommande;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->Position;
    }

    public function setPosition(string $Position): self
    {
        $this->Position = $Position;

        return $this;
    }

    public function getLigneCommande(): ?LigneCommande
    {
        return $this->ligneCommande;
    }

    public function setLigneCommande(?LigneCommande $ligneCommande): self
    {
        // unset the owning side of the relation if necessary
        if ($ligneCommande === null && $this->ligneCommande !== null) {
            $this->ligneCommande->setCustomization(null);
        }

        // set the owning side of the relation if necessary
        if ($ligneCommande !== null && $ligneCommande->getCustomization() !== $this) {
            $ligneCommande->setCustomization($this);
        }

        $this->ligneCommande = $ligneCommande;

        return $this;
    }
}
