<?php

namespace App\DataFixtures;

use App\Entity\AdresseCommandes;
use App\Entity\AdresseUtilisateur;
use App\Entity\Utilisateur;
use App\Entity\Commande;
use App\Entity\Declinaison;
use App\Entity\Image;
use App\Entity\AdresseCommande;
use App\Entity\Facturation;
use App\Entity\LigneCommande;
use App\Entity\Produit;
use App\Entity\TVA;
use App\Entity\Customization;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use App\Faker\ProviderCollection;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        ProviderCollection::addProdiverTo($faker);
        $faker->seed(8080);

        $prixProd = Array();//Des produits
        $Prod= Array();
        //Produits et Déclinaisons
        $produit = Array();
        for($i = 0; $i < 100; ++$i){
            $produit[$i] = new Produit();
            $produit[$i]->setLibelleProduit($faker->libelleProduct);
            $produit[$i]->setDescription($faker->description);
            $produit[$i]->setPrix($faker->prix);
            $produit[$i]->setImage($faker->image);
            $produit[$i]->setPersonnalisable($faker->personnalisable);

            $manager->persist($produit[$i]);
            $manager->flush();

            $decli = Array();
            for($y=0; $y < 3 ; ++$y){
                $decli[$y]= new Declinaison();
                $decli[$y]->setColoris($faker->hexColor);
                $decli[$y]->setLibelle($faker->colorName);
                $decli[$y]->setIdProduit($produit[$i]);

                $manager->persist($decli[$y]);
                $manager->flush();

                $prixProd[$decli[$y]->getId()]=$produit[$i]->getPrix();
                array_push($Prod,$decli[$y]);
            }
        }

        //Utilisateurs
        $users = Array();
        $usersAdr = Array();
        for($i = 0; $i<10 ; ++$i){
            $usersAdr[$i] = new AdresseUtilisateur;
            $usersAdr[$i]->setTel($faker->mobileNumber);
            $usersAdr[$i]->setPays("France");
            $usersAdr[$i]->setCodePostale($faker->departmentNumber);
            $usersAdr[$i]->setVille("Orange");
            $usersAdr[$i]->setLibelleAdresse($faker->numero." ".$faker->prerue." ".$faker->rue);

            $users[$i] = new Utilisateur;
            $users[$i]->setNomClient($faker->lastName);
            $users[$i]->setPrenomClient($faker->firstname);
            $users[$i]->addAdresseUtilisateur($usersAdr[$i]);
            $users[$i]->setTel($faker->mobileNumber);
            $users[$i]->setPassword($faker->password);
            $users[$i]->setEmail($faker->safeEmail);
            $users[$i]->setRole("user");

            $usersAdr[$i]->setUser($users[$i]);

            $manager->persist($users[$i]);
            $manager->persist($usersAdr[$i]);
            $manager->flush();

            //Commande d'uilisateurs (50% de chance)
            if (rand(1, 10) > 5) {
                $commande =new Commande;
                $commande->setDateCommande(date_format($faker->dateTimeThisYear,"d/m/Y"));
                $commande->setDateMinEstime(date_format($faker->dateTimeThisYear,"d/m/Y"));
                $commande->setDateMaxEstime(date_format($faker->dateTimeThisYear,"d/m/Y"));
                $commande->setPrixTotal(0);
                $commande->setUtilisateur($users[$i]);

                $manager->persist($commande);
                $manager->flush();

                //produit commandé
                $MonProd =$Prod[rand(0,sizeof($Prod)-1)];

                $ligneCommande = new LigneCommande;
                $ligneCommande->setQuantite(1);
                $ligneCommande->setDeclinaison($MonProd);
                $ligneCommande->setCommande($commande);

                //peut-être customisation
                if(rand(1,10)>5){
                    $custom = new Customization();
                    $custom->setPosition($faker->position);
                    $custom->setType($faker->type);

                    $manager->persist($custom);
                    $manager->flush();

                    $ligneCommande->setCustomization($custom);
                    $ligneCommande->setPersonnalisation($faker->personnalisation);
                }
                else{
                    $ligneCommande->setCustomization(null);
                    $ligneCommande->setPersonnalisation("");
                }
                $commande->setPrixTotal($commande->getPrixTotal()+$prixProd[$MonProd->getId()]);

                //Facturation et Livraison
                $facturation = new AdresseCommandes();
                $livraison = new AdresseCommandes();

                $facturation->setType('facturation');
                $livraison->setType('livraison');

                $facturation->setCommande($commande);
                $livraison->setCommande($commande);

                $facturation->setPays("France");
                $livraison->setPays("France");

                $facturation->setCodePostale($faker->departement);
                $livraison->setCodePostale($faker->departement);

                $facturation->setVille($faker->ville);
                $livraison->setVille($faker->ville);

                $facturation->setLibelleAdresse($faker->numero." ".$faker->prerue." ".$faker->rue);
                $livraison->setLibelleAdresse($faker->numero." ".$faker->prerue." ".$faker->rue);

                $facturation->setTel($users[$i]->getTel());
                $livraison->setTel($users[$i]->getTel());

                $manager->persist($commande);
                $manager->persist($ligneCommande);
                $manager->persist($facturation);
                $manager->persist($livraison);
            }
        }
        $manager->flush();
    }
}