"use strict";

let CONNEXION_FORM =
    $('<div />').append(
        $('<form />')
            .attr("id", "logform")
            .attr("method", "post")
            .attr("action", "http://localhost/account/login")
            .append(
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "email")
                            .attr("for", "email")
                            .html("Email :"),
                        $('<input />')
                            .attr("type", "email")
                            .attr("id", "email")
                            .attr("name", "email")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "password")
                            .attr("for", "password")
                            .html("Password :"),
                        $('<input />')
                            .attr("type", "password")
                            .attr("id", "password")
                            .attr("name", "password")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .append(
                        $('<br>'),
                        $('<input />')
                            .attr("class", "btn btn-primary")
                            .attr("type", "submit")
                            .attr("value", "Se connecter")
                    ),
            )
    )


let NEWACCOUNT_FORM =
    $('<div />').append(
        $('<form />')
            .attr("id", "newform")
            .attr("method", "post")
            .attr("action", "http://localhost/account/new")
            .append(
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "nom")
                            .html("Nom :"),
                        $('<input />')
                            .attr("type", "nom")
                            .attr("name", "nom")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "prenom")
                            .html("Prénom :"),
                        $('<input />')
                            .attr("type", "prenom")
                            .attr("name", "prenom")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "rue")
                            .html("Rue :"),
                        $('<input />')
                            .attr("type", "rue")
                            .attr("name", "rue")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "cp")
                            .html("Code postale :"),
                        $('<input />')
                            .attr("type", "cp")
                            .attr("name", "cp")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "commune")
                            .html("Commune :"),
                        $('<input />')
                            .attr("type", "commune")
                            .attr("name", "commune")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "tel")
                            .html("Téléphone :"),
                        $('<input />')
                            .attr("type", "tel")
                            .attr("name", "tel")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "email")
                            .html("Email :"),
                        $('<input />')
                            .attr("type", "email")
                            .attr("name", "email")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .attr("class", "form-group")
                    .append(
                        $('<label />')
                            .attr("htmlFor", "password")
                            .html("Mot de passe :"),
                        $('<input />')
                            .attr("type", "password")
                            .attr("name", "password")
                            .attr("class", "form-control")
                            .attr("required", "required")
                    ),
                $('<div />')
                    .append(
                        $('<br>'),
                        $('<input />')
                            .attr("class", "btn btn-primary")
                            .attr("type", "submit")
                            .attr("value", "Créer un compte")
                    ),
            )
    )

$(() => {
    $('.connectaccount')
        .click(function () {
            $('#formtoaccount_content')
                .empty()
                .append(
                    CONNEXION_FORM
                )
            $('.connectaccount')
                .attr("id", "accounton")
            $('.newaccount')
                .attr("id", "accountoff")
        })
    $('.newaccount')
        .click(function () {
            $('#formtoaccount_content')
                .empty()
                .append(
                    NEWACCOUNT_FORM
                )
            $('.connectaccount')
                .attr("id", "accountoff")
            $('.newaccount')
                .attr("id", "accounton")
        })
});