<?php

namespace App\Tests;

use App\Controller\PanierController;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class AppTest extends TestCase
{
    /**
     * @test
     * @dataProvider getData
     */
    public function AreWorking($test){
        $this->assertEquals('a', $test,'hello world !');
    }

    public function getData()
    {
        return [
            'test1'=> [
                'a'
            ],
            'test2'=>[
                'b'
            ]
        ];
    }
}